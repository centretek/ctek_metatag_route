<?php

namespace Drupal\ctek_metatag_route\Plugin\metatag\Tag;

use Drupal\metatag\Annotation\MetatagTag;
use Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * @MetatagTag(
 *   id="json_ld",
 *   name="",
 *   label=@Translation("JSON+LD"),
 *   description=@Translation("Outputs JSON+LD"),
 *   group="advanced",
 *   weight=0,
 *   type="application/ld+json",
 *   secure=FALSE,
 *   multiple=FALSE,
 *   long=TRUE
 * )
 */
class JsonLD extends MetaNameBase {

  public function form(array $element = []) {
    $form = parent::form($element);
    unset($form['#maxlength']);
    return $form;
  }

  public function output() {
    if (empty($this->value)) {
      return '';
    }
    return [
      '#tag' => 'script',
      '#attributes' => [
        'type' => $this->type,
      ],
      '#value' => $this->value,
    ];
  }

}

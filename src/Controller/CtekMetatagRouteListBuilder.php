<?php

namespace Drupal\ctek_metatag_route\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List builder implementation for custom metatag routes.
 *
 * @package Drupal\ctek_metatag_route\Controller
 */
class CtekMetatagRouteListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['route'] = $entity->getRoute();
    $row += parent::buildRow($entity);
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'route' => t('Route Name'),
    ] + parent::buildHeader();
    return $header;
  }

}

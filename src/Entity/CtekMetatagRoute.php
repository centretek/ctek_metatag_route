<?php

namespace Drupal\ctek_metatag_route\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Define the custom metatag route entity.
 *
 * @ConfigEntityType(
 *   id = "ctek_metatag_route",
 *   label = @Translation("Custom Metatag Route"),
 *   handlers = {
 *     "list_builder" = "Drupal\ctek_metatag_route\Controller\CtekMetatagRouteListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ctek_metatag_route\Form\CtekMetatagRouteAddForm",
 *       "edit" = "Drupal\ctek_metatag_route\Form\CtekMetatagRouteEditForm",
 *       "delete" = "Drupal\ctek_metatag_route\Form\CtekMetatagRouteDeleteForm",
 *     }
 *   },
 *   config_prefix = "route",
 *   admin_permission = "administer ctek metatag routes",
 *   entity_keys = {
 *     "id" = "route",
 *     "label" = "route",
 *   },
 *   config_export = {
 *     "route",
 *     "tags",
 *   },
 *   links = {
 *     "collection" = "/admin/config/search/metatag/custom",
 *     "edit-form" = "/admin/config/search/metatag/custom/{ctek_metatag_route}",
 *     "delete-form" = "/admin/config/search/metatag/custom/{ctek_metatag_route}/delete",
 *   }
 * )
 */
class CtekMetatagRoute extends ConfigEntityBase implements CtekMetatagRouteInterface {

  /**
   * Route ID.
   *
   * @var string
   */
  public $route;

  /**
   * {@inheritDoc}
   */
  public function id() {
    return isset($this->route) ? $this->route : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getRoute() {
    return $this->route;
  }

  /**
   * {@inheritDoc}
   */
  public function getTags() {
    return isset($this->tags) ? $this->tags : [];
  }

}

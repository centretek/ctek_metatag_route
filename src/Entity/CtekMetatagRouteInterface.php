<?php

namespace Drupal\ctek_metatag_route\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for accessing custom metatag route entities.
 *
 * @package Drupal\ctek_metatag_route\Entity
 */
interface CtekMetatagRouteInterface extends ConfigEntityInterface {

  /**
   * Fetch the route id.
   *
   * @return string
   *   Route ID.
   */
  public function getRoute();

  /**
   * Fetch the metatag information.
   *
   * @return array
   *   Dictionary of metatag names and values.
   */
  public function getTags();

}

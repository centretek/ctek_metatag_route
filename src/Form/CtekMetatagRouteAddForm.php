<?php

namespace Drupal\ctek_metatag_route\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\PathValidator;
use Drupal\Core\Routing\RouteProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Form for creating and editing CtekMetatagRoute entities.
 *
 * @package Drupal\ctek_metatag_route\Form
 */
class CtekMetatagRouteAddForm extends EntityForm {

  /**
   * Route Provider.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  private $routeProvider;

  /**
   * Path Validator.
   *
   * @var \Drupal\Core\Path\PathValidator
   */
  private $pathValidator;

  /**
   * Constructs an ExampleForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   * @param \Drupal\Core\Routing\RouteProvider $routeProvider
   *   The route provider.
   * @param \Drupal\Core\Path\PathValidator $pathValidator
   *   The path validator.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RouteProvider $routeProvider, PathValidator $pathValidator) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeProvider = $routeProvider;
    $this->pathValidator = $pathValidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('router.route_provider'),
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\ctek_metatag_route\Entity\CtekMetatagRouteInterface $cmr */
    $cmr = $this->entity;

    $form['route'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Route ID or URL'),
      '#maxlength' => 255,
      '#default_value' => $cmr->getRoute(),
      '#description' => $this->t("Route ID where metatags will be added. Providing an internal path (starting with '/') will attempt to find the matching route id."),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $route_name = $form_state->getValue('route');

    try {
      // If the value is not a route name, this will throw an exception.
      $this->routeProvider->getRouteByName($route_name);
    }
    catch (RouteNotFoundException $e) {

      $url = trim($route_name);

      if (strpos($url, '/') === FALSE) {
        $form_state->setErrorByName('route', $this->t('Unrecognized route name or internal path. Internal paths must begin with /.'));
        return FALSE;
      }

      $url_object = $this->pathValidator->getUrlIfValid($url);
      if ($url_object) {
        $route_name = $url_object->getRouteName();
      }
      else {
        $form_state->setErrorByName('route', $this->t('Unrecognized route name or internal path. Internal paths must begin with /.'));
        return FALSE;
      }
    }

    // Validate that the route doesn't have an entity already.
    $ids = $this->entityTypeManager
      ->getStorage('ctek_metatag_route')
      ->getQuery()
      ->condition('route', $route_name)
      ->execute();

    if ($ids) {
      $form_state->setErrorByName('route', $this->t('There are already metatags created for this route (%route).', [
        '%route' => $route_name,
      ]));
      return FALSE;
    }

    $form_state->setValue('route', $route_name);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $cmr = $this->entity;
    $status = $cmr->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved %label.', [
        '%label' => $cmr->label(),
      ]));

      $form_state->setRedirectUrl($cmr->toUrl('edit-form'));
    }
    else {
      $this->messenger()->addMessage($this->t('%label was not saved.', [
        '%label' => $cmr->label(),
      ]), MessengerInterface::TYPE_ERROR);

      $form_state->setRedirect('entity.ctek_metatag_route.collection');
    }
  }

}
